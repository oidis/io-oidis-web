/* ********************************************************************************************************* *
 *
 * Copyright 2020 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

var appExceptionHandler = function ($message, $stack) {
    console.log($message);
    if ($stack !== undefined && $stack !== "" && $stack !== null) {
        console.log($stack);
    }
};
var errorHandler = function ($message, $filename, $lineno, $colno, $error) {
    if ($message === "") {
        console.log("Package \"<? @var clientConfig.packageName ?>\" has not been loaded.");
    } else {
        $message =
            "       <b>" + $message + "</b><br>" +
            "       file: " + $filename + "<br>" +
            "       at line: " + $lineno;
        var stack = null;
        if ($error) {
            $message += ":" + $colno;
            stack = $error.stack;
        }
        appExceptionHandler($message, stack);
    }
    return true;
};
var scriptHandler = function ($onSuccess) {
    var loader = document.createElement("script");
    loader.src = "resource/javascript/<? @var properties.packageName ?>.min.js?v=<? @var build.timestamp ?>";
    loader.type = "text/javascript";

    var timeoutId = null;
    var cleanUp = function () {
        var passed = true;
        try {
            if (timeoutId !== null) {
                clearTimeout(timeoutId);
            }
            if (loader.parentNode !== undefined) {
                loader.parentNode.removeChild(loader);
            }
        } catch (ex) {
            passed = false;
            appExceptionHandler("Failed to clean up ScriptHandler.");
        }
        return passed;
    };
    window.onerror = function () {
        return true;
    };
    loader.onerror = function ($error) {
        window.onerror = errorHandler;
        if (cleanUp()) {
            if ($error.message === undefined) {
                appExceptionHandler("Failed to load resource from: " + loader.src + ".");
            } else {
                appExceptionHandler($error);
            }
        }
        return true;
    };

    var loaded = false;
    var onloadHandler = function () {
        window.onerror = errorHandler;
        if (cleanUp()) {
            try {
                if (!loaded) {
                    loaded = true;
                    $onSuccess();
                }
            } catch (ex) {
                appExceptionHandler(ex.message, ex.stack);
            }
        }
    };
    loader.onload = onloadHandler;
    loader.onreadystatechange = function () {
        if (loader.readyState === "complete" || loader.readyState === "loaded") {
            onloadHandler();
        }
    };

    cleanUp();
    timeoutId = setTimeout(function () {
        if (cleanUp()) {
            appExceptionHandler("Timeout reached");
        }
    }, 2500);
    document.body.appendChild(loader);
};

try {
    window.onerror = errorHandler;
    window.onload = function () {
        scriptHandler(function () {
                var loaderClass = "<? @var clientConfig.loaderClass ?>";
                if (loaderClass === "") {
                    loaderClass = "Io.Oidis.Commons.Loader";
                }
                var loaderClassParts = loaderClass.split(".");
                var classObject = window;
                for (var index = 0; index < loaderClassParts.length; index++) {
                    if (classObject[loaderClassParts[index]] !== undefined) {
                        classObject = classObject[loaderClassParts[index]];
                    }
                }
                try {
                    Io.Oidis.Commons.Primitives.String = Io.Oidis.Commons.Utils.StringUtils;
                    classObject.Load("<? @var clientConfig ?>");
                } catch (ex) {
                    appExceptionHandler(ex.message, ex.stack);
                }
            }
        );
    };
} catch (ex) {
    appExceptionHandler(ex.message, ex.stack);
}
