/* ********************************************************************************************************* *
 *
 * Copyright 2020 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Web.HttpProcessor {
    "use strict";
    import HttpRequestConstants = Io.Oidis.Gui.Enums.HttpRequestConstants;

    export class HttpResolver extends Io.Oidis.Gui.HttpProcessor.HttpResolver {

        protected getStartupResolvers() : any {
            const resolvers : any = {
                /* tslint:disable: object-literal-sort-keys */
                "/"                  : Io.Oidis.Web.Index,
                "/{section}"         : Io.Oidis.Web.Index,
                "/PersistenceManager": null
                /* tslint:enable */
            };
            resolvers["/ServerError/Exception/{" + HttpRequestConstants.EXCEPTION_TYPE + "}"] = Io.Oidis.Web.Index;
            return resolvers;
        }

        protected getRequestParserClass() : any {
            return HttpRequestParser;
        }
    }
}
