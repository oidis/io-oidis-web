/* ********************************************************************************************************* *
 *
 * Copyright 2020 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Web.HttpProcessor {
    "use strict";
    import ArrayList = Io.Oidis.Commons.Primitives.ArrayList;

    export class HttpRequestParser extends Io.Oidis.Commons.HttpProcessor.HttpRequestParser {
        protected getRawHeaders() : Io.Oidis.Commons.Primitives.ArrayList<string> {
            return new ArrayList<string>();
        }
    }
}
