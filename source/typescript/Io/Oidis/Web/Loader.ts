/* ********************************************************************************************************* *
 *
 * Copyright 2020 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Web {
    "use strict";
    import HttpResolver = Io.Oidis.Web.HttpProcessor.HttpResolver;

    export class Loader extends Io.Oidis.Commons.Loader {

        public static getInstance() : Loader {
            return <Loader>super.getInstance();
        }

        public getHttpResolver() : HttpResolver {
            return <HttpResolver>super.getHttpResolver();
        }

        protected initResolver() : HttpResolver {
            return new HttpResolver("/");
        }

        protected initEnvironment() : EnvironmentArgs {
            return new EnvironmentArgs();
        }
    }
}
