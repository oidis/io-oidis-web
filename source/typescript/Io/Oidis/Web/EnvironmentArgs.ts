/* ********************************************************************************************************* *
 *
 * Copyright 2020 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Web {
    "use strict";

    export class EnvironmentArgs extends Io.Oidis.Commons.EnvironmentArgs {
        protected getConfigPaths() : string[] {
            return [];
        }
    }
}
