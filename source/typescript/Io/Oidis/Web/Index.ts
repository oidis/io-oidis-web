/* ********************************************************************************************************* *
 *
 * Copyright 2020 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Web {
    "use strict";
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import ElementEventsManager = Io.Oidis.Gui.Events.ElementEventsManager;
    import MouseEventArgs = Io.Oidis.Gui.Events.Args.MouseEventArgs;
    import WindowManager = Io.Oidis.Gui.Utils.WindowManager;
    import ElementManager = Io.Oidis.Gui.Utils.ElementManager;
    import ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;
    import ArrayList = Io.Oidis.Commons.Primitives.ArrayList;
    import HttpRequestConstants = Io.Oidis.Commons.Enums.HttpRequestConstants;
    import Exception = Io.Oidis.Commons.Exceptions.Type.Exception;
    import ObjectEncoder = Io.Oidis.Commons.Utils.ObjectEncoder;
    import KeyEventHandler = Io.Oidis.Gui.Utils.KeyEventHandler;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;
    import EventArgs = Io.Oidis.Commons.Events.Args.EventArgs;
    import BrowserType = Io.Oidis.Commons.Enums.BrowserType;

    export class Index extends Io.Oidis.Commons.HttpProcessor.Resolvers.BaseHttpResolver {
        private static loaded : boolean;
        private goTo : string;
        private bubbles : HTMLElement[];
        private visible : HTMLElement[];

        protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
            if ($POST.KeyExists(HttpRequestConstants.EXCEPTIONS_LIST)) {
                LogIt.Error((<ArrayList<Exception>>$POST.getItem(HttpRequestConstants.EXCEPTIONS_LIST)).ToString("", false));
            }
            if ($GET.KeyExists("section")) {
                this.goTo = $GET.getItem("section");
            }
        }

        protected resolver() : void {
            if (!Index.loaded) {
                Index.loaded = true;
                this.init();
            } else {
                this.navigate();
            }
        }

        private init() : void {
            this.bubbles = [];
            this.visible = [];
            ElementManager.getElement("Anime").childNodes.forEach(($node : HTMLElement) : void => {
                if (!ObjectValidator.IsEmptyOrNull($node) && $node.nodeType === 1) {
                    this.bubbles.push($node);
                    this.visible.push($node);
                }
            });
            this.animateBubbles();

            WindowManager.getEvents().setOnScroll(() : void => {
                this.menu();
            });
            this.getEventsManager().FireAsynchronousMethod(() : void => {
                this.menu();
            }, 10);
            this.goToTopButton();
            this.moreInfoButton();
            this.mediatelButton();

            this.navigate();
        }

        private navigate() : void {
            if (!ObjectValidator.IsEmptyOrNull(this.goTo)) {
                const sections : any = {
                    "kontakt": "ContactPanel",
                    "o-nas"  : "AboutUsPanel",
                    "sluzby" : "ServicesPanel"
                };
                if (sections.hasOwnProperty(this.goTo)) {
                    this.scrollTo(sections[this.goTo], WindowManager.getSize().Width() < 500 ? 60 : 0);
                }
            } else {
                this.scrollTo();
            }
        }

        private menu() : void {
            const scroll : number = this.getScroll();
            let style : string = "Menu";
            if (scroll > 80) {
                const height : number = WindowManager.getSize().Height();

                style += " Scroll";
                if (scroll < height - 100) {
                    style += " Top";
                }

                ElementManager.setOpacity("MoreInfo", (scroll < height - 500) ? 100 : 0);
            }
            ElementManager.setClassName("Menu", style);
        }

        private goToTopButton() : void {
            const events : ElementEventsManager = new ElementEventsManager("GoToTop");
            events.setOnClick(() : void => {
                this.getHttpManager().ReloadTo("/#");
            });
            events.Subscribe();
        }

        private moreInfoButton() : void {
            const events : ElementEventsManager = new ElementEventsManager("MoreInfo");
            events.setOnClick(() : void => {
                this.scrollTo("IntroPanel");
            });
            events.Subscribe();
        }

        private mediatelButton() : void {
            const phone : HTMLInputElement = <HTMLInputElement>ElementManager.getElement("Phone");
            const email : HTMLInputElement = <HTMLInputElement>ElementManager.getElement("Email");
            const zip : HTMLInputElement = <HTMLInputElement>ElementManager.getElement("ZIP");

            const validatePhone : any = () : boolean => {
                return StringUtils.Length(StringUtils.Remove(phone.value, "+", " ")) >= 9;
            };
            const validateZIP : any = () : boolean => {
                return StringUtils.Length(StringUtils.Remove(zip.value, " ")) === 5;
            };
            const validateEmail : any = () : boolean => {
                return StringUtils.Contains(email.value, "@") && StringUtils.Contains(email.value, ".");
            };

            phone.onkeydown = ($events : KeyboardEvent) : void => {
                try {
                    if (!KeyEventHandler.IsEdit($events) && !KeyEventHandler.IsNavigate($events)) {
                        if ($events.key.match(/[\+\s0-9]/g) === null ||
                            StringUtils.Length(StringUtils.Remove(phone.value, "+", " ")) > 11) {
                            $events.preventDefault();
                        }
                    }
                } catch (ex) {
                    LogIt.Error(ex.message, ex);
                }
            };
            zip.onkeydown = ($events : KeyboardEvent) : void => {
                try {
                    if (!KeyEventHandler.IsEdit($events) && !KeyEventHandler.IsNavigate($events)) {
                        if ($events.key.match(/[\s0-9]/g) === null ||
                            StringUtils.Length(StringUtils.Remove(zip.value, " ")) > 4) {
                            $events.preventDefault();
                        }
                    }
                } catch (ex) {
                    LogIt.Error(ex.message, ex);
                }
            };

            const items : string[] = ["Company", "Address", "Phone", "City", "ZIP", "Email"];
            items.forEach(($target : string) : void => {
                const element : HTMLInputElement = (<HTMLInputElement>ElementManager.getElement($target));
                element.onfocus = () : void => {
                    (<HTMLElement>element.parentNode).className = "Item Active";
                };
                element.onblur = () : void => {
                    (<HTMLElement>element.parentNode).className = "Item";
                };
            });

            const events : ElementEventsManager = new ElementEventsManager("MediatelSend");
            events.setOnClick(($eventArgs : EventArgs) : void => {
                $eventArgs.PreventDefault();
                let passed : boolean = true;
                const values : any = {};
                items.forEach(($target : string) : void => {
                    values[$target] = (<HTMLInputElement>ElementManager.getElement($target)).value;
                    if (ObjectValidator.IsEmptyOrNull(values[$target]) ||
                        $target === "Phone" && !validatePhone() ||
                        $target === "ZIP" && !validateZIP() ||
                        $target === "Email" && !validateEmail()) {
                        ElementManager.setClassName($target, "Error");
                        passed = false;
                    } else {
                        ElementManager.setClassName($target, "");
                        values[$target] = ObjectEncoder.Url(values[$target]);
                    }
                });
                if (passed) {
                    this.getHttpManager().ReloadTo("https://" +
                        (this.getEnvironmentArgs().IsProductionMode() ? "mediatel.cz" : "mediatel.koder.cz") +
                        "/sluzby/sprava-zobrazovani-firmy/uprava-zapisu/?" +
                        "company_name=" + values.Company + "&" +
                        "address=" + values.Address + "&" +
                        "phone=" + values.Phone + "&" +
                        "city=" + values.City + "&" +
                        "zip=" + values.ZIP + "&" +
                        "contact_email=" + values.Email + "&" +
                        "utm_source=oidis.org&utm_medium=cpa&utm_campaign=mext", true);
                }
            });
            events.Subscribe();
        }

        private animateBubbles() : void {
            if (this.getRequest().getBrowserType() !== BrowserType.INTERNET_EXPLORER) {
                const events : ElementEventsManager = new ElementEventsManager("Header");
                const timeouts : any = {};
                let prevX : number = null;
                events.setOnMouseMove(($args : MouseEventArgs) : void => {
                    const x : number = $args.NativeEventArgs().offsetX;
                    const y : number = $args.NativeEventArgs().offsetY;
                    let toLeft : boolean = false;
                    if (!ObjectValidator.IsEmptyOrNull(prevX) && prevX > x) {
                        toLeft = true;
                    }
                    prevX = x;
                    let index : number = 0;
                    this.bubbles.forEach(($bubble : HTMLElement) : void => {
                        if (this.visible.Contains($bubble)) {
                            const bubbleX : number = $bubble.offsetLeft;
                            const bubbleY : number = $bubble.offsetTop;
                            if (bubbleX < (x + 100) && bubbleX > (x - 100) &&
                                bubbleY < (y + 100) && bubbleY > (y - 100)) {
                                this.getEventsManager().FireAsynchronousMethod(() : void => {
                                    if (toLeft) {
                                        $bubble.style.left = (bubbleX - ($bubble.offsetWidth + 300)) + "px";
                                    } else {
                                        $bubble.style.left = (bubbleX + ($bubble.offsetWidth + 300)) + "px";
                                    }
                                });
                                if (!ObjectValidator.IsEmptyOrNull(timeouts[index])) {
                                    clearTimeout(timeouts[index]);
                                }
                                timeouts[index] = this.getEventsManager().FireAsynchronousMethod(() : void => {
                                    $bubble.style.left = "";
                                    timeouts[index] = null;
                                }, 500);
                            }
                        }
                        index++;
                    });
                });
                events.Subscribe();
                ElementManager.Show("Anime");
                this.generateBubble();
            }
        }

        private generateBubble($index : number = 31) : void {
            this.getEventsManager().FireAsynchronousMethod(() : void => {
                try {
                    let hideId : number = $index - 30;
                    if (hideId < 0) {
                        hideId += 100;
                    }
                    if (!ObjectValidator.IsEmptyOrNull(this.bubbles[hideId])) {
                        this.bubbles[hideId].style.display = "none";
                        this.bubbles[hideId].style.left = "";
                        this.visible.splice(hideId, 1);
                    }
                    if (this.bubbles.length < 100) {
                        const newBubble : HTMLElement = document.createElement("div");
                        this.visible.push(newBubble);
                        this.bubbles.push(newBubble);
                        ElementManager.getElement("Anime").appendChild(newBubble);
                    } else if (!ObjectValidator.IsEmptyOrNull(this.bubbles[$index])) {
                        this.bubbles[$index].style.display = "block";
                        this.visible.push(this.bubbles[$index]);
                    }
                    if ($index < 100) {
                        $index++;
                    } else {
                        $index = 0;
                    }

                    const bubble : HTMLElement = this.bubbles[this.getRandomInt(0, this.bubbles.length - 1)];
                    if (this.visible.Contains(bubble)) {
                        const maxMove : number = this.getRandomInt(0, 300);
                        const direction : number = (maxMove > 0) ? 1 : -1;
                        const moveBubble : any = ($index : number = 0) : void => {
                            if ($index < 15) {
                                this.getEventsManager().FireAsynchronousMethod(() : void => {
                                    bubble.style.left = (bubble.offsetLeft + this.getRandomInt(0, Math.abs(maxMove)) * direction) + "px";
                                    moveBubble($index + 1);
                                }, false, 100);
                            } else {
                                bubble.style.left = "";
                            }
                        };
                        moveBubble();
                    }
                } catch (ex) {
                    LogIt.Error(ex.message);
                }
                this.generateBubble($index);
            }, 500);
        }

        private getRandomInt($min : number, $max : number) : number {
            $min = Math.ceil($min);
            $max = Math.floor($max);
            return Math.floor(Math.random() * ($max - $min + 1)) + $min;
        }

        private getScroll() : number {
            if (!ObjectValidator.IsEmptyOrNull(document.documentElement)) {
                return document.documentElement.scrollTop;
            }
            return document.body.scrollTop;
        }

        private scrollTo($target? : string, $offset : number = 0) : void {
            let value : number = 0;
            if (!ObjectValidator.IsEmptyOrNull($target)) {
                value = ElementManager.getAbsoluteOffset($target).Top() - 40 - $offset;
            }
            window.scrollTo(0, value);
            this.menu();
        }
    }
}
