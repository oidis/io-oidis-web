/* ********************************************************************************************************* *
 *
 * Copyright 2020 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* tslint:disable:no-reference */
/// <reference path="../../dependencies/io-oidis-commons/source/typescript/reference.d.ts" />
/// <reference path="../../dependencies/io-oidis-gui/source/typescript/reference.d.ts" />
/// <reference path="../../dependencies/io-oidis-services/source/typescript/reference.d.ts" />

// generated-code-start
/// <reference path="Io/Oidis/Web/EnvironmentArgs.ts" />
/// <reference path="Io/Oidis/Web/HttpProcessor/HttpRequestParser.ts" />
/// <reference path="Io/Oidis/Web/HttpProcessor/HttpResolver.ts" />
/// <reference path="Io/Oidis/Web/Loader.ts" />
/// <reference path="Io/Oidis/Web/Index.ts" />
// generated-code-end
