# io-oidis-web v2020.1.0

> Project focused on Oidis identity presentation

## Requirements

This project does not have any special requirements but it depends on the 
[Oidis Builder](https://gitlab.com/oidis/io-oidis-builder). See the Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the 
[Oidis Builder](https://gitlab.com/oidis/io-oidis-builder) documentation.

## Documentation

This project provides automatically-generated documentation in [TypeDoc](http://typedoc.org/) from the TypeScript source by running the 
`wui docs` command from the {projectRoot} folder.

> NOTE: The documentation is accessible also from the {projectRoot}/build/target/docs/index.html file after a successful creation.

## History

### v2020.1.0
Web and content redesign.
### v2020.0.0
Content update. Usage of configuration in jsonp format.
### v2019.3.0
Initial release

## License

This software is owned or controlled by Oidis. 
Use of this software is governed by the BSD-3-Clause License distributed with this material.
  
See the `LICENSE.txt` file distributed for more details.

---

Author Jakub Cieslar,
Copyright 2019-2020 [Oidis](https://www.oidis.org/)
